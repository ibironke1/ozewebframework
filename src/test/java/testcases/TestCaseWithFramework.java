package testcases;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.BaseClass;
import pages.LoginPage;
import utility.ExcelDataProvider;


public class TestCaseWithFramework extends BaseClass{



    @Test(dataProvider = "test1data")
    public void test1(String username, String password) throws InterruptedException {
        System.out.println ("The details are " + username+" | "+password);
         LoginPage loginPage = PageFactory.initElements (driver, LoginPage.class);
        Thread.sleep ( 2000 );

    }

    @DataProvider(name = "test1data")
    public Object[][] getData(){
        String excelPath = "TestData/Data.xlsx";
        Object data[][] = testData (excelPath, "LoginData");
        return data;
    }

    public Object[][] testData(String excelPath, String sheet){
        ExcelDataProvider excel = new ExcelDataProvider ();
        int rowCount = excel.getRowCount ();
        int colCount = excel.getColCount ();

        Object data[][] = new Object[rowCount-1][colCount];

        for (int i = 1; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {

                String cellData = excel.getUsernameCellDataString ( i, j );
                System.out.print ( cellData + " |" );
                data[i-1][j] = cellData;
            }
            System.out.println ();
        }
        return data;
    }



    @Test
   public void LoginApp(){
        /**
        ExcelDataProvider excel=new ExcelDataProvider ();
        LoginPage loginPage = PageFactory.initElements (driver, LoginPage.class);
        loginPage.loginToWebApp (excel.getStringData ( "LoginData",1,1),excel.getStringData ( "LoginData", 2,2));
**/
    }
}