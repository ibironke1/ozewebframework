package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class TestCaseWithoutFramework {
    @Test
    public void test1(){
        //Set the browser path to find it
        System.setProperty ("webdriver.chrome.driver", "Drivers/chromedriver");
        //Start the browser
        WebDriver driver = new ChromeDriver ();
        //Maximize the browser driver
        driver.manage ().window ().maximize ();
        //Input the website URL
        driver.get ( "https://app-test.oze.guru/login" );
        //Wait for 30seconds to get it fully loaded
        driver.manage ().timeouts ().implicitlyWait ( 30, TimeUnit.SECONDS );
        //Input in the username in the username field
        driver.findElement ( By.name ("username")).sendKeys ( "8141414141" );
        //Input the password in the password field
        driver.findElement ( By.name ("password")).sendKeys ( "Testers@1" );
        //Click on the login button
        driver.findElement ( By.xpath ("//*[@id=\"loginform\"]/div[3]/button")).click ();
        //Make the dashboard display for 30 seconds
        driver.manage ().timeouts ().implicitlyWait ( 30, TimeUnit.SECONDS );
        //Quit the browser
        driver.quit ();
    }
}
