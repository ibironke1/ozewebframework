package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import utility.BrowserFactory;
import utility.ExcelDataProvider;

public class BaseClass {

    public WebDriver driver;
    public ExcelDataProvider excel;


    @BeforeSuite
    public void setUpSuite() {
        excel = new ExcelDataProvider ();
    }

    @BeforeClass
    public void setUp() {
        driver = BrowserFactory.startApplication ( driver, "Chrome", "https://app-test.oze.guru/login" );
    }


    @AfterClass
    public void tearDown() {
        BrowserFactory.quitBrowser ( driver );
    }


    @BeforeTest
    public void startApplication() {
        System.setProperty ( "webdriver.chrome.driver", "Drivers/chromedriver" );
        driver = new ChromeDriver ();
    }
}


