package utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ExcelTest {
    WebDriver driver=null;

    @BeforeTest
    public void startApplication(){
            System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");
            driver = new ChromeDriver ();
        }

    @Test(dataProvider = "test1data")
    public void test1(String username, String password) throws InterruptedException {
        System.out.println ("The details are "+username+" | "+password);
        driver.get ("https://opensource-demo.orangehrmlive.com/");
        driver.findElement (By.id ("txtUsername")).sendKeys(username);
        driver.findElement (By.id ("txtPassword")).sendKeys( password );
        Thread.sleep ( 2000 );
    }

    @DataProvider(name = "test1data")
    public Object[][] getData(){
        String excelPath = "TestData/Data.xlsx";
        Object data[][] = testData (excelPath, "LoginData");
        return data;
    }

    public Object[][] testData(String excelPath, String sheet){
        ExcelDataProvider excel = new ExcelDataProvider ();
        int rowCount = excel.getRowCount ();
        int colCount = excel.getColCount ();

        Object data[][] = new Object[rowCount-1][colCount];

        for (int i = 1; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {

                String cellData = excel.getUsernameCellDataString ( i, j );
                System.out.print ( cellData + " |" );
                data[i-1][j] = cellData;
            }
            System.out.println ();
        }
        return data;
    }
}

