package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class BrowserFactory {


    @BeforeTest
    public static WebDriver startApplication(WebDriver driver, String browserName, String appUrl){
        //check if the browsername is Chrome in other to launch Chromedriver
        if (browserName.equals("Chrome")){
            //Fetch the Browser path / location
            System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");
            //Instantiate WebDriver object
            driver = new ChromeDriver ();
        }
        //check if the browsername is Firefox in other to launch Geckodriver
        else if(browserName.equals("Firefox")){
            //Browser setup for InternetExplorer
            System.setProperty("webdriver.gecko.driver","Drivers/geckodriver");
            //Instantiate WebDriver object
            driver = new FirefoxDriver ();
        }
        //check if the browsername is Safari in other to launch SafariDriver
        else if(browserName.equals("Safari")){
            //Browser setup for SafariDriver
            System.setProperty("webdriver.safari.driver","Drivers/SafariDriver");
            //Instantiate WebDriver object
            driver = new SafariDriver ();
        }
        //If none of the browser listed for test is available, display a non-support message
        else {
            System.out.println("We do not support this browser");
        }
        //Wait for 30 seconds for the browser to full load
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        //After loading fully, maximixe the browser window
        driver.manage().window().maximize();
        //Input the web url
        driver.get(appUrl);
        //Wait for an additional 30seconds for the website to fully load
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //Wait for the driver to be returned
        return driver;
    }

    @AfterTest
    public static void quitBrowser(WebDriver ldriver){
        //quit the driver after test
        ldriver.quit();
    }
}
