package utility;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataProvider {

    static XSSFWorkbook workbook;
    static XSSFSheet sheet;

    public ExcelDataProvider(){
       try {
           workbook = new XSSFWorkbook ("TestData/Data.xlsx");
            sheet = workbook.getSheet ("LoginData");
        }catch (Exception e){
            e.printStackTrace ();
        }
    }

    public static void main(String[] args) {
        getRowCount ();
        //getUsernameCellDataString (1,0);
        //getPasswordCellDataString (1,1);
    }

    public static int getRowCount() {
        int rowCount=0;
        try {
            rowCount = sheet.getPhysicalNumberOfRows ();
            System.out.println ( "The number of rows: " + rowCount );

        } catch (Exception exp) {
            System.out.println ( exp.getMessage () );
            System.out.println ( exp.getCause () );
            exp.printStackTrace ();
        }
        return rowCount;
    }

    public static int getColCount() {
        int colCount = 0;
        try {
            colCount = sheet.getRow (0).getPhysicalNumberOfCells ();
            System.out.println ( "The number of Columns: " + colCount );

        } catch (Exception exp) {
            System.out.println ( exp.getMessage () );
            System.out.println ( exp.getCause () );
            exp.printStackTrace ();
        }
        return colCount;
    }

    public static String getUsernameCellDataString(int rowNum, int columnNum) {
        String cellData=null;
        try {
            cellData = sheet.getRow(rowNum).getCell(columnNum).getStringCellValue();
//            System.out.println(cellData);

        } catch (Exception exp) {
            System.out.println ( exp.getMessage () );
            System.out.println ( exp.getCause () );
            exp.printStackTrace ();
        }
        return cellData;
    }

    public static String getPasswordCellDataString(int rowNum, int columnNum) {
        String cellData2=null;
        try {
            cellData2 =sheet.getRow(rowNum).getCell(columnNum).getStringCellValue();
 //           System.out.println ( cellData2 );

        } catch (Exception exp) {
            System.out.println ( exp.getMessage () );
            System.out.println ( exp.getCause () );
            exp.printStackTrace ();
        }
        return cellData2;
    }
}

/**
    public static void main(String[] args){

    }
    XSSFWorkbook wb;

    public ExcelDataProvider(){

        File src=new File ( "TestData/Data.xlsx" );

        try {
            FileInputStream fis= new FileInputStream (src);
            wb = new XSSFWorkbook ( fis );
        } catch (IOException e){
            System.out.println ("Unable to read excel file "+e.getMessage ());
        }

    }

    public String getStringData(int sheetIndex, int row, int column){
        return wb.getSheetAt(sheetIndex).getRow(row).getCell(column).getStringCellValue ();
    }

    public String getStringData(String sheetName, int row, int column){
       return wb.getSheet(sheetName).getRow(row).getCell(column).getStringCellValue ();
    }

    public double getNumericData(String sheetName, int row, int column){
        return wb.getSheet(sheetName).getRow(row).getCell(column).getNumericCellValue ();

    }

}
 **/